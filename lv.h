#ifndef LVMDEFRAG_LV_H
#define LVMDEFRAG_LV_H 1

#include "list.h"

#include "extents.h"

#include <stdbool.h>

struct lv
{
	/** other LVs */
	struct list_node other_lvs;

	/* uuid */
	char *uuid;

	/** name */
	char *name;

	/** size in extents */
	extents_t size;

	/** segments in this LV before defrag */
	struct list_node segments;

	/** segments in this LV after defrag */
	struct list_node segments_after;
};

extern struct list_node lvs;

struct lv *find_lv_by_uuid(char const *uuid);
struct lv *find_lv_by_name(char const *name);
struct lv *create_lv(char const *uuid, char const *name);

bool scan_lvs(void *lvm);

#endif
