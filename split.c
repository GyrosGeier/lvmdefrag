#ifdef HAVE_CONFIG_H
#include <config.h>
#endif

#include "split.h"

#include <stddef.h>

char *split(char *str, char sep, char **saveptr)
{
	char *begin = str ? str : *saveptr;

	if(!begin)
		return NULL;

	for(char *cursor = begin; cursor[0] != '\0'; ++cursor)
	{
		if(cursor[0] == sep)
		{
			cursor[0] = '\0';
			*saveptr = cursor + 1;
			return begin;
		}
	}
	*saveptr = NULL;
	return begin;
}
