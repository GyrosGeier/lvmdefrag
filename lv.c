#ifdef HAVE_CONFIG_H
#include <config.h>
#endif

#include "lv.h"

#include "split.h"

#include <lvm2cmd.h>

#include <stdlib.h>

#include <string.h>

struct list_node lvs = { &lvs, &lvs };

struct lv *create_lv(char const *uuid, char const *name)
{
	struct lv *new_lv = malloc(sizeof *new_lv);
	if(!new_lv)
		abort();

	new_lv->uuid = strdup(uuid);
	new_lv->name = strdup(name);
	new_lv->size = 0;
	list_init(&new_lv->segments);

	list_append(&lvs, &new_lv->other_lvs);

	return new_lv;
}

struct lv *find_lv_by_uuid(char const *uuid)
{
	LIST_FOREACH(struct lv, lv, &lvs, other_lvs)
		if(!strcmp(uuid, lv->uuid))
			return lv;

	return NULL;
}

struct lv *find_lv_by_name(char const *name)
{
	LIST_FOREACH(struct lv, lv, &lvs, other_lvs)
		if(!strcmp(name, lv->name))
			return lv;

	return NULL;
}

static void add_lv(
		int level,
		const char *file,
		int line,
		int dm_errno,
		const char *message)
{
	/* unused */
	(void)file;
	(void)line;
	(void)dm_errno;

	if(level != LVM2_LOG_PRINT)
		/* TODO: handle errors */
		return;

	if(!strncmp(message, "WARNING:", 8))
		/* LVM2 bug: warning message with LVM2_LOG_PRINT */
		return;

	/* strip away constness -- this is bad style */
	char *const record = (char *)message;

	/* saveptr for split */
	char *saveptr_1;

	/* split columns */
	char *const lv_name = split(record, '=', &saveptr_1);
	char *const lv_uuid = split(NULL, '=', &saveptr_1);

	if(!lv_name || !lv_uuid)
		abort();

	/* endptr for strtoext */
	//char *endptr;

	/*
	extents_t const pv_pe_count_n = strtoextents(pv_pe_count, &endptr, 10);
	if(endptr[0] != '\0')
		abort();

	extents_t const pv_pe_alloc_count_n = strtoextents(pv_pe_alloc_count, &endptr, 10);
	if(endptr[0] != '\0')
		abort();
	 */

	if(find_lv_by_uuid(lv_uuid))
		/* no duplicates! */
		abort();

	/* struct lv *new_lv = */ create_lv(lv_uuid, lv_name);
}

bool scan_lvs(void *lvm)
{
	lvm2_log_fn(&add_lv);

	lvm2_run(lvm, "lvs --noheadings --separator = --all --options lv_name,lv_uuid");

	return true;
}
