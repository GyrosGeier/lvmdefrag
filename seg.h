#ifndef LVMDEFRAG_SEG_H
#define LVMDEFRAG_SEG_H 1

#include "extents.h"

#include "list.h"

#include <stdbool.h>

struct pv;
struct lv;

enum backing_type
{
	PV,
	LV
};

struct seg
{
	/** size */
	extents_t size;

	/** other copies of the segment */
	struct list_node other_copies;

	/** LV this segment belongs to */
	struct lv *lv;

	/** start offset of this segment in the LV */
	extents_t lv_offset;

	/** other segments in same LV */
	struct list_node other_segments_in_lv;

	/** underlying data storage (PV or LV) */
	enum backing_type backing_type;

	/** depending on backing_type */
	union
	{
		/** valid if backing_type == PV */
		struct {
			/** PV this segment is stored in */
			struct pv *pv;

			/** start offset of this segment in the PV */
			extents_t pv_offset;

			/** other segments in same PV */
			struct list_node other_segments_in_pv;
		} pv_old, pv_new;
	};
};

extern struct list_node segments;

bool scan_segments(void *lvm);

#endif
