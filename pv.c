#ifdef HAVE_CONFIG_H
#include <config.h>
#endif

#include "pv.h"

#include "vg.h"

#include "split.h"

#include <lvm2cmd.h>

#include <string.h>

struct list_node unused_pvs = { &unused_pvs, &unused_pvs };

struct pv *create_pv(char const *uuid, char const *name, struct vg *in_vg)
{
	struct pv *new_pv = malloc(sizeof *new_pv);
	if(!new_pv)
		abort();

	new_pv->uuid = strdup(uuid);
	new_pv->name = name ? strdup(name) : NULL;
	new_pv->size = 0;
	new_pv->used = 0;

	list_init(&new_pv->segments_old);
	list_init(&new_pv->segments_new);

	if(in_vg)
		list_append(&in_vg->pvs, &new_pv->other_pvs);
	else
		list_append(&unused_pvs, &new_pv->other_pvs);

	return new_pv;
}

struct pv *find_pv_in_vg_by_uuid(struct vg *vg, char const *uuid)
{
	if(vg)
	{
		LIST_FOREACH(struct pv, pv, &vg->pvs, other_pvs)
			if(!strcmp(pv->uuid, uuid))
				return pv;
	}
	else
	{
		LIST_FOREACH(struct pv, pv, &unused_pvs, other_pvs)
			if(!strcmp(pv->uuid, uuid))
				return pv;
	}
	return NULL;
}

struct pv *find_pv_in_vg_by_name(struct vg *vg, char const *name)
{
	if(vg)
	{
		LIST_FOREACH(struct pv, pv, &vg->pvs, other_pvs)
			if(pv->name && !strcmp(pv->name, name))
				return pv;
	}
	else
	{
		LIST_FOREACH(struct pv, pv, &unused_pvs, other_pvs)
			if(pv->name && !strcmp(pv->name, name))
				return pv;
	}
	return NULL;
}

struct pv *find_pv_by_uuid(char const *uuid)
{
	struct pv *pv = find_pv_in_vg_by_uuid(NULL, uuid);
	if(pv)
		return pv;

	LIST_FOREACH(struct vg, vg, &vgs, other_vgs)
	{
		pv = find_pv_in_vg_by_uuid(vg, uuid);
		if(pv)
			return pv;
	}

	return NULL;
}

struct pv *find_pv_by_name(char const *name)
{
	struct pv *pv = find_pv_in_vg_by_name(NULL, name);
	if(pv)
		return pv;

	LIST_FOREACH(struct vg, vg, &vgs, other_vgs)
	{
		pv = find_pv_in_vg_by_name(vg, name);
		if(pv)
			return pv;
	}

	return NULL;
}

static void add_pv(
		int level,
		const char *file,
		int line,
		int dm_errno,
		const char *message)
{
	/* unused */
	(void)file;
	(void)line;
	(void)dm_errno;

	if(level != LVM2_LOG_PRINT)
		/* TODO: handle errors */
		return;

	if(!strncmp(message, "WARNING:", 8))
		/* LVM2 bug: warning message with LVM2_LOG_PRINT */
		return;

	/* strip away constness -- this is bad style */
	char *const record = (char *)message;

	/* saveptr for split */
	char *saveptr_1;

	/* split columns */
	char *const pv_name = split(record, '=', &saveptr_1);
	char *const pv_uuid = split(NULL, '=', &saveptr_1);
	char *const vg_uuid = split(NULL, '=', &saveptr_1);
	char *const pv_missing = split(NULL, '=', &saveptr_1);
	char *const pv_pe_count = split(NULL, '=', &saveptr_1);
	char *const pv_pe_alloc_count = split(NULL, '=', &saveptr_1);

	if(!pv_name || !pv_uuid || !pv_missing || !pv_pe_count || !pv_pe_alloc_count)
		abort();

	if(pv_uuid[0] == '\0')
		/* not a PV, since it doesn't have a UUID */
		return;

	bool const pv_missing_n = (pv_missing[0] != '\0');

	/* endptr for strtoext */
	char *endptr;

	extents_t const pv_pe_count_n = strtoextents(pv_pe_count, &endptr, 10);
	if(endptr[0] != '\0')
		abort();

	extents_t const pv_pe_alloc_count_n = strtoextents(pv_pe_alloc_count, &endptr, 10);
	if(endptr[0] != '\0')
		abort();

	if(find_pv_by_uuid(pv_uuid))
		/* no duplicates! */
		abort();

	struct vg *in_vg = NULL;

	if(vg_uuid[0] != '\0')
	{
		in_vg = find_vg_by_uuid(vg_uuid);
		if(!in_vg)
			/* inconsistent setup */
			abort();
	}

	struct pv *new_pv = create_pv(pv_uuid,
			pv_missing_n ? NULL : pv_name,
			in_vg);

	new_pv->size = pv_pe_count_n;
	new_pv->used = pv_pe_alloc_count_n;
}

bool scan_pvs(void *lvm)
{
	lvm2_log_fn(&add_pv);

	lvm2_run(lvm, "pvs --noheadings --separator = --all --options pv_name,pv_uuid,vg_uuid,pv_missing,pv_pe_count,pv_pe_alloc_count");

	return true;
}
