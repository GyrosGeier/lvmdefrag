#ifndef LVMDEFRAG_PV_H
#define LVMDEFRAG_PV_H 1

#include "extents.h"

#include "list.h"

#include <stdbool.h>

extern struct list_node unused_pvs;

struct pv
{
	/** other PVs */
	struct list_node other_pvs;

	/** uuid */
	char *uuid;

	/** name */
	char *name;

	/** size in extents */
	extents_t size;

	/** used extents */
	extents_t used;

	/** segments, in order before defrag */
	struct list_node segments_old;

	/** segments, in order after defrag */
	struct list_node segments_new;
};

bool scan_pvs(void *lvm);

struct vg;

struct pv *find_pv_in_vg_by_uuid(struct vg *vg, char const *uuid);
struct pv *find_pv_in_vg_by_name(struct vg *vg, char const *name);
struct pv *find_pv_by_uuid(char const *uuid);
struct pv *find_pv_by_name(char const *name);
struct pv *create_pv(char const *uuid, char const *name, struct vg *in_vg);

#endif
