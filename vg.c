#ifdef HAVE_CONFIG_H
#include <config.h>
#endif

#include "vg.h"

#include "split.h"

#include <lvm2cmd.h>

#include <stdlib.h>
#include <string.h>

struct list_node vgs = { &vgs, &vgs };

struct vg *find_vg_by_uuid(char const *uuid)
{
	LIST_FOREACH(struct vg, vg, &vgs, other_vgs)
		if(!strcmp(vg->uuid, uuid))
			return vg;
	return NULL;
}

struct vg *find_vg_by_name(char const *name)
{
	LIST_FOREACH(struct vg, vg, &vgs, other_vgs)
		if(!strcmp(vg->name, name))
			return vg;
	return NULL;
}

struct vg *create_vg(char const *uuid, char const *name)
{
	if(find_vg_by_uuid(uuid) || find_vg_by_name(name))
		/* no duplicates */
		abort();

	struct vg *new_vg = malloc(sizeof *new_vg);
	
	if(!new_vg)
		abort();

	new_vg->uuid = strdup(uuid);
	new_vg->name = strdup(name);

	list_init(&new_vg->pvs);
	list_init(&new_vg->lvs);

	list_append(&vgs, &new_vg->other_vgs);

	return new_vg;
}

static void add_vg(
		int level,
		const char *file,
		int line,
		int dm_errno,
		const char *message)
{
	/* unused */
	(void)file;
	(void)line;
	(void)dm_errno;

	if(level != LVM2_LOG_PRINT)
		/* TODO: handle errors */
		return;

	if(!strncmp(message, "WARNING:", 8))
		/* LVM2 bug: warning message with LVM2_LOG_PRINT */
		return;

	/* strip away constness -- this is bad style */
	char *const record = (char *)message;

	/* saveptr for split */
	char *saveptr_1;

	/* split columns */
	char *const vg_name = split(record, '=', &saveptr_1);
	char *const vg_uuid = split(NULL, '=', &saveptr_1);

	if(!vg_name || !vg_uuid)
		abort();

	/* struct vg *new_vg = */ create_vg(vg_uuid, vg_name);
}

bool scan_vgs(void *lvm)
{
	lvm2_log_fn(&add_vg);

	lvm2_run(lvm, "vgs --noheadings --separator = --all --options vg_name,vg_uuid");

	return true;
}
