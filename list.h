#ifndef LVMDEFRAG_LIST_H
#define LVMDEFRAG_LIST_H 1

/** @file list.h
 *
 * @brief standard double linked list
 *
 * The list header and each list element contain a list_node. An empty list
 * is represented by a list_node pointing to itself in both the next and prev
 * fields.
 */

#include <stddef.h>
#include <stdbool.h>

/** Double-linked list linkage node */
struct list_node
{
	struct list_node *next, *prev;
};

/** Initialize empty list */
static inline void list_init(
		struct list_node *list)
{
	list->next = list->prev = list;
}

/** Check if a list is empty */
static inline bool list_empty(struct list_node *list)
{
	if(list->next == list)
		return true;
	return false;
}

/** Insert before an element */
static inline void list_insert_before(
		struct list_node *pos,
		struct list_node *elem)
{
	elem->next = pos;
	elem->prev = pos->prev;
	pos->prev->next = elem;
	pos->prev = elem;
}

/** Append one element at the end of the list */
static inline void list_append(
		struct list_node *list,
		struct list_node *elem)
{
	/* insert before the list header node */
	list_insert_before(list, elem);
}

static inline void list_swap(
		struct list_node *a,
		struct list_node *b)
{
	/* special cases */
	if(a->next == b)
	{
		b->next->prev = a;
		a->prev->next = b;
		b->prev = a->prev;
		a->next = b->next;
		b->next = a;
		a->prev = b;
	}
	else if(b->next == a)
	{
		a->next->prev = b;
		b->prev->next = a;
		a->prev = b->prev;
		b->next = a->next;
		a->next = b;
		b->prev = a;
	}
	else
	{
		struct list_node tmp = { a->next, a->prev };
		a->next->prev = b;
		a->prev->next = b;
		b->next->prev = a;
		b->prev->next = a;
		a->next = b->next;
		a->prev = b->prev;
		b->next = tmp.next;
		b->prev = tmp.prev;
	}
}

/* same parameter order as in Linux */
#define container_of(ptr, type, member) \
	((type *)(((char *)(ptr)) - offsetof(type, member)))

#define LIST_FOREACH(type, name, list, member) \
	type *name; \
	for( \
			struct list_node *node = (list)->next; \
			name = container_of(node, type, member), node != (list); \
			node = node->next)

#endif
