#ifndef LVMDEFRAG_VG_H
#define LVMDEFRAG_VG_H 1

#include "list.h"

#include <stdbool.h>

struct vg
{
	struct list_node other_vgs;

	char *uuid;
	char *name;

	struct list_node pvs;
	struct list_node lvs;
};

extern struct list_node vgs;

bool scan_vgs(void *lvm);

struct vg *find_vg_by_uuid(char const *uuid);
struct vg *find_vg_by_name(char const *name);
struct vg *create_vg(char const *uuid, char const *name);


#endif
