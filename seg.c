#ifdef HAVE_CONFIG_H
#include <config.h>
#endif

#include "seg.h"

#include "pv.h"
#include "lv.h"

#include "split.h"

#include <lvm2cmd.h>

#include <string.h>

#include <stdio.h>	/* TODO: remove */

#include <assert.h>

struct list_node segments;

static void add_segment(
		int level,
		const char *file,
		int line,
		int dm_errno,
		const char *message)
{
	/* unused */
	(void)file;
	(void)line;
	(void)dm_errno;

	if(level != LVM2_LOG_PRINT)
		/* TODO: handle errors */
		return;

	if(!strncmp(message, "WARNING:", 8))
		/* LVM2 bug: warning message with LVM2_LOG_PRINT */
		return;

	/* strip away constness -- this is bad style */
	char *const record = (char *)message;

	/* saveptr for split */
	char *saveptr_1;

	/* split columns */
	char *const seg_type = split(record, '=', &saveptr_1);
	char *const lv_uuid = split(NULL, '=', &saveptr_1);
	char *const pv_uuid = split(NULL, '=', &saveptr_1);
	char *const seg_start_pe = split(NULL, '=', &saveptr_1);
	char *const seg_size_pe = split(NULL, '=', &saveptr_1);
	char *const pvseg_start = split(NULL, '=', &saveptr_1);

	if(!seg_type || !lv_uuid || !pv_uuid || !seg_start_pe || !seg_size_pe || !pvseg_start)
		abort();

	if(!strcmp(seg_type, "free"))
		/* ignore free segments for now */
		return;

	if(strcmp(seg_type, "linear"))
		/* only linear mappings handled for now */
		abort();

	/* endptr for strtoext */
	char *endptr;

	extents_t const seg_start_pe_n = strtoextents(seg_start_pe, &endptr, 10);
	if(endptr[0] != '\0')
		abort();

	extents_t const seg_size_pe_n = strtoextents(seg_size_pe, &endptr, 10);
	if(endptr[0] != '\0')
		abort();

	extents_t const pvseg_start_n = strtoextents(pvseg_start, &endptr, 10);
	if(endptr[0] != '\0')
		abort();

	struct lv *lv = find_lv_by_uuid(lv_uuid);
	if(!lv)
		abort();

	struct pv *pv = find_pv_by_uuid(pv_uuid);

	struct seg *new_seg = malloc(sizeof *new_seg);
	if(!new_seg)
		abort();

	new_seg->size = seg_size_pe_n;

	new_seg->lv = lv;
	new_seg->lv_offset = seg_start_pe_n;

	if(pv)
	{
		new_seg->backing_type = PV;

		new_seg->pv_old.pv = pv;
		new_seg->pv_old.pv_offset = pvseg_start_n;

		list_append(&pv->segments_old, &new_seg->pv_old.other_segments_in_pv);
	}
	else
	{
		new_seg->backing_type = LV;
	}

	/* insert at appropriate place in LV, keeping it sorted. */

	/* insertion will happen before this point, which is the end of the list. */
	struct list_node *insertion_point = &lv->segments;

	for(struct list_node *i = lv->segments.next;
			i != &lv->segments;
			i = i->next)
	{
		struct seg *seg = container_of(i, struct seg, other_segments_in_lv);

		if(seg->lv_offset < new_seg->lv_offset)
			continue;
		if(seg->lv_offset == new_seg->lv_offset)
		{
			/* found a copy */
			list_append(&seg->other_copies, &new_seg->other_copies);
			return;
		}

		/* other paths have branched off */
		assert(seg->lv_offset > new_seg->lv_offset);

		if(insertion_point != &lv->segments)
		{
			struct seg *ips = container_of(insertion_point, struct seg, other_segments_in_lv);
			if(seg->lv_offset < ips->lv_offset)
				insertion_point = i;
		}
		else
		{
			insertion_point = i;
		}
	}

	/* first copy, initialize list of other copies */
	list_init(&new_seg->other_copies);

	/* insert at appropriate point */
	list_insert_before(insertion_point, &new_seg->other_segments_in_lv);

	/* add size to LV size (can't query for that, so we determine that as we go */
	lv->size += seg_size_pe_n;
}

bool scan_segments(void *lvm)
{
	lvm2_log_fn(&add_segment);

	lvm2_run(lvm, "pvs --noheadings --separator = --all --options seg_type,lv_uuid,pv_uuid,seg_start_pe,seg_size_pe,pvseg_start");

	return true;
}
