#ifndef LVMDEFRAG_EXTENTS_H
#define LVMDEFRAG_EXTENTS_H 1

#include <stdlib.h>

/* sensible type for size in extents, plus wrappers so no one needs to know
 * the definition
 */

typedef unsigned long long extents_t;

static inline extents_t strtoextents(
		char const *s,
		char **endptr,
		int base)
{
	return strtoull(s, endptr, base);
}

#define PRIextents "%llu"

#endif
