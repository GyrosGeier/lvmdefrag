#ifdef HAVE_CONFIG_H
#include <config.h>
#endif

#include "seg.h"
#include "lv.h"
#include "vg.h"
#include "pv.h"

#include <lvm2cmd.h>

#include <stdlib.h>
#include <stdio.h>

/*
static void sort_segments(struct pv *pv)
{
	for(struct list_node *i = pv->segments.next; i != &pv->segments; i = i->next)
	{
		struct seg *is = container_of(i, struct seg, other_segments_in_pv);
		for(struct list_node *j = i->next; j != &pv->segments; j = j->next)
		{
			struct seg *js = container_of(j, struct seg, other_segments_in_pv);
			if(is->pv_offset > js->pv_offset)
			{
				list_swap(i, j);
				i = j;
				is = container_of(i, struct seg, other_segments_in_pv);
			}
		}
	}
}
 */

int main(int argc, char **argv)
{
	/* unused */
	(void)argc;
	(void)argv;

	void *lvm = lvm2_init();

	if(!lvm)
	{
		fprintf(stderr, "E: cannot initialize LVM\n");
		exit(1);
	}

	scan_vgs(lvm);
	scan_pvs(lvm);
	scan_lvs(lvm);
	scan_segments(lvm);

	if(!list_empty(&unused_pvs))
	{
		printf("Unused PVs:\n");
		LIST_FOREACH(struct pv, pv, &unused_pvs, other_pvs)
			printf("  %s\n", pv->name);
	}

	LIST_FOREACH(struct vg, vg, &vgs, other_vgs)
	{
		printf("VG %s:\n", vg->name);

		printf("  PV layout:\n");
		LIST_FOREACH(struct pv, pv, &vg->pvs, other_pvs)
		{
			if(pv->size == 0)
				continue;

			printf("    %s (" PRIextents "/" PRIextents "):", pv->name, pv->used, pv->size);

			extents_t last_extent = 0;
		
			LIST_FOREACH(struct seg, seg, &pv->segments_old, pv_old.other_segments_in_pv)
			{
				if(last_extent > seg->pv_old.pv_offset)
					/* not properly sorted */
					abort();
				if(last_extent < seg->pv_old.pv_offset)
					printf(" -(" PRIextents ")", seg->pv_old.pv_offset - last_extent);
				printf(" %s(" PRIextents ")", seg->lv->name, seg->size);
				last_extent = seg->pv_old.pv_offset + seg->size;
			}

			if(last_extent > pv->size)
				/* inconsistent data */
				abort();

			if(last_extent < pv->size)
				printf(" -(" PRIextents ")", pv->size - last_extent);

			printf("\n");
		}

		printf("  LV layout:\n");
		LIST_FOREACH(struct lv, lv, &lvs, other_lvs)
		{
			printf("    %s (" PRIextents "):", lv->name, lv->size);

			extents_t last_extent = 0;

			LIST_FOREACH(struct seg, seg, &lv->segments, other_segments_in_lv)
			{ 
				if(last_extent > seg->lv_offset)
					/* not properly sorted */
					abort();
				if(last_extent < seg->lv_offset)
					/* segment missing */
					printf(" MISSING(" PRIextents ")", seg->size);

				printf(" %s@" PRIextents "(" PRIextents ")",
						seg->backing_type == PV ? seg->pv_old.pv->name : "(LV)",
						seg->pv_old.pv_offset,
						seg->size);
				last_extent = seg->lv_offset + seg->size;
			}
			printf("\n");
		}
	}

	lvm2_exit(lvm);
	exit(0);
}
